/*
 * RDL - Robot Dynamics Library
 * Copyright (c) 2017 Jordan Lack <jlack1987@gmail.com>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#ifndef __RDL_FORCE_VECTOR_HPP__
#define __RDL_FORCE_VECTOR_HPP__

/**
 * @file TransformableGeometricObject.hpp
 * @brief Contains various geometric objects that have methods for transforming themselves into different frames of reference
 */

#include "rdl_dynamics/rdl_eigenmath.h"
#include "rdl_dynamics/SpatialAlgebraOperators.h"

namespace RobotDynamics
{
namespace Math
{
/**
 * @class ForceVector
 * @brief A ForceVector is a SpatialVector containing 3 moments and 3 linear forces.
 */
class ForceVector : public SpatialVector, public TransformableGeometricObject
{
  public:
    /**
     * @brief Constructor
     * @param other
     */
    template <typename OtherDerived>
    // cppcheck-suppress noExplicitConstructor
    ForceVector(const Eigen::MatrixBase<OtherDerived>& other) : SpatialVector(other)
    {
    }

    /**
     * Overloaded equal operator
     * @param other
     * @return Copied ForceVector
     */
    template <typename OtherDerived>
    ForceVector& operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        SpatialVector::operator=(other);
        return *this;
    }

    /**
     * @brief Empty constructor
     */
    EIGEN_STRONG_INLINE ForceVector() : SpatialVector(0., 0., 0., 0., 0., 0.)
    {
    }

    /**
     * @brief Get a copy of a ForceVector as type SpatialVector
     */
    EIGEN_STRONG_INLINE SpatialVector toSpatialVector() const
    {
        return *this;
    }

    /**
     * @brief Constructor
     * @param mx x-Moment
     * @param my y-Moment
     * @param mz z-Moment
     * @param fx x-Linear Force
     * @param fy y-Linear Force
     * @param fz z-Linear Force
     */
    ForceVector(const double mx, const double my, const double mz, const double fx, const double fy, const double fz)
    {
        Base::_check_template_params();

        (*this) << mx, my, mz, fx, fy, fz;
    }

    /**
     * @brief Setter
     * @param f
     */
    EIGEN_STRONG_INLINE void set(const ForceVector& f)
    {
        (*this) << f.data()[0], f.data()[1], f.data()[2], f.data()[3], f.data()[4], f.data()[5];
    }

    /**
     * @brief Get reference to x-angular component
     * @return Reference to x-angular component
     */
    EIGEN_STRONG_INLINE double& mx()
    {
        return this->data()[0];
    }

    /**
     * @brief Get reference to y-angular component
     * @return Reference to y-angular component
     */
    EIGEN_STRONG_INLINE double& my()
    {
        return this->data()[1];
    }

    /**
     * @brief Get reference to z-angular component
     * @return Reference to z-angular component
     */
    EIGEN_STRONG_INLINE double& mz()
    {
        return this->data()[2];
    }

    /**
     * @brief Get copy of x-angular component
     * @return Copy of x-angular component
     */
    EIGEN_STRONG_INLINE double mx() const
    {
        return this->data()[0];
    }

    /**
     * @brief Get copy of y-angular component
     * @return Copy of y-angular component
     */
    EIGEN_STRONG_INLINE double my() const
    {
        return this->data()[1];
    }

    /**
     * @brief Get copy of z-angular component
     * @return Copy of z-angular component
     */
    EIGEN_STRONG_INLINE double mz() const
    {
        return this->data()[2];
    }

    /**
     * @brief Get reference to x-linear component
     * @return Reference to x-linear component
     */
    EIGEN_STRONG_INLINE double& fx()
    {
        return this->data()[3];
    }

    /**
     * @brief Get reference to y-linear component
     * @return Reference to y-linear component
     */
    EIGEN_STRONG_INLINE double& fy()
    {
        return this->data()[4];
    }

    /**
     * @brief Get reference to z-linear component
     * @return Reference to z-linear component
     */
    EIGEN_STRONG_INLINE double& fz()
    {
        return this->data()[5];
    }

    /**
     * @brief Get copy of x-linear component
     * @return Copy of x-linear component
     */
    EIGEN_STRONG_INLINE double fx() const
    {
        return this->data()[3];
    }

    /**
     * @brief Get copy of y-linear component
     * @return Copy of y-linear component
     */
    EIGEN_STRONG_INLINE double fy() const
    {
        return this->data()[4];
    }

    /**
     * @brief Get copy of z-linear component
     * @return Copy of z-linear component
     */
    EIGEN_STRONG_INLINE double fz() const
    {
        return this->data()[5];
    }

    /**
     * @brief Transform a force vector. Performs \f$ f= X^T*f \f$
     * @param X SpatialTransform
     */
    inline void transformTranspose(const SpatialTransform& X)
    {
        this->set(this->transformTranspose_copy(X));
    }

    /**
     * @brief Copies then transforms a ForceVector
     * @param X
     * @return Returns a copied, transform ForceVector
     */
    ForceVector transformTranspose_copy(const SpatialTransform& X) const
    {
        Vector3d E_T_f(X.E(0, 0) * this->data()[3] + X.E(1, 0) * this->data()[4] + X.E(2, 0) * this->data()[5],
                       X.E(0, 1) * this->data()[3] + X.E(1, 1) * this->data()[4] + X.E(2, 1) * this->data()[5],
                       X.E(0, 2) * this->data()[3] + X.E(1, 2) * this->data()[4] + X.E(2, 2) * this->data()[5]);

        return ForceVector(X.E(0, 0) * this->data()[0] + X.E(1, 0) * this->data()[1] + X.E(2, 0) * this->data()[2] - X.r[2] * E_T_f[1] + X.r[1] * E_T_f[2],
                           X.E(0, 1) * this->data()[0] + X.E(1, 1) * this->data()[1] + X.E(2, 1) * this->data()[2] + X.r[2] * E_T_f[0] - X.r[0] * E_T_f[2],
                           X.E(0, 2) * this->data()[0] + X.E(1, 2) * this->data()[1] + X.E(2, 2) * this->data()[2] - X.r[1] * E_T_f[0] + X.r[0] * E_T_f[1], E_T_f[0],
                           E_T_f[1], E_T_f[2]);
    }

    /**
     * @brief Performs the following in place transform \f[
     * f =
     * \begin{bmatrix}
     * X.E & -X.E(X.r\times) \\
     * \mathbf{0} & X.E
     * \end{bmatrix}
     * f
     * \f]
     * @param X
     */
    void transform(const SpatialTransform& X)
    {
        this->set(this->transform_copy(X));
    }

    /**
     * @brief Copy then transform a ForceVector by \f[
     * f_2 =
     * \begin{bmatrix}
     * X.E & -X.E(X.r\times) \\
     * \mathbf{0} & X.E
     * \end{bmatrix}
     * f_1
     * \f]
     * @param X
     * @return Copied, transformed ForceVector
     */
    ForceVector transform_copy(const SpatialTransform& X) const
    {
        Vector3d En_rxf = X.E * (this->getAngularPart() - X.r.cross(this->getLinearPart()));

        return ForceVector(En_rxf[0], En_rxf[1], En_rxf[2], X.E(0, 0) * this->data()[3] + X.E(0, 1) * this->data()[4] + X.E(0, 2) * this->data()[5],
                           X.E(1, 0) * this->data()[3] + X.E(1, 1) * this->data()[4] + X.E(1, 2) * this->data()[5],
                           X.E(2, 0) * this->data()[3] + X.E(2, 1) * this->data()[4] + X.E(2, 2) * this->data()[5]);
    }

    /**
     * @brief Overloaded plus-equals operator
     * @return \f$ f_2=f_2+f_1 \f$
     */
    inline ForceVector operator+=(const ForceVector& v)
    {
        (*this) << (this->mx() += v.mx()), (this->my() += v.my()), (this->mz() += v.mz()), (this->fx() += v.fx()), (this->fy() += v.fy()), (this->fz() += v.fz());
        return *this;
    }
};

/**
 * @brief Operator for transforming a ForceVector. Calls the ForceVector::transform method.
 * @param X SpatialTransform
 * @param f ForceVector to be transformed
 * @return Transformed ForceVector
 */
inline ForceVector operator*(const SpatialTransform& X, ForceVector f)
{
    f.transform(X);
    return f;
}
}  // namespace Math
}  // namespace RobotDynamics

#endif