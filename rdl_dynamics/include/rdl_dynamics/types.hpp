/*
 * RDL - Robot Dynamics Library
 * Copyright (c) 2019 Jordan Lack <jlack1987@gmail.com>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#ifndef __RDL_TYPES_HPP__
#define __RDL_TYPES_HPP__

#include <memory>

#define URDF_TYPEDEF_CLASS_POINTER(Class)                                                                                                                                \
    class Class;                                                                                                                                                         \
    typedef std::shared_ptr<Class> Class##Ptr;                                                                                                                           \
    typedef std::shared_ptr<const Class> Class##ConstPtr;                                                                                                                \
    typedef std::weak_ptr<Class> Class##WeakPtr

namespace RobotDynamics
{
URDF_TYPEDEF_CLASS_POINTER(Model);
URDF_TYPEDEF_CLASS_POINTER(ReferenceFrame);
}  // namespace RobotDynamics

#endif